package com.android.mobile.pathfinder;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        ViewInteraction appCompatEditText = onView(
                withId(R.id.row_value));
        appCompatEditText.perform(scrollTo(), replaceText("5"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                withId(R.id.column_value));
        appCompatEditText2.perform(scrollTo(), replaceText("1"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.submit_size), withText("submit size")));
        appCompatButton.perform(scrollTo(), click());

        ViewInteraction appCompatEditText3 = onView(
                withId(R.id.element_value));
        appCompatEditText3.perform(scrollTo(), replaceText("1"), closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.submit_element_value), withText("Submit Element Value")));
        appCompatButton2.perform(scrollTo(), click());

        ViewInteraction appCompatEditText4 = onView(
                withId(R.id.element_value));
        appCompatEditText4.perform(scrollTo(), replaceText("51"), closeSoftKeyboard());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.submit_element_value), withText("Submit Element Value")));
        appCompatButton3.perform(scrollTo(), click());

        ViewInteraction appCompatEditText5 = onView(
                withId(R.id.element_value));
        appCompatEditText5.perform(scrollTo(), replaceText("7"), closeSoftKeyboard());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.submit_element_value), withText("Submit Element Value")));
        appCompatButton4.perform(scrollTo(), click());

        ViewInteraction appCompatEditText6 = onView(
                withId(R.id.element_value));
        appCompatEditText6.perform(scrollTo(), replaceText("4"), closeSoftKeyboard());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.submit_element_value), withText("Submit Element Value")));
        appCompatButton5.perform(scrollTo(), click());

        ViewInteraction appCompatEditText7 = onView(
                withId(R.id.element_value));
        appCompatEditText7.perform(scrollTo(), replaceText("3"), closeSoftKeyboard());

        ViewInteraction appCompatButton6 = onView(
                allOf(withId(R.id.submit_element_value), withText("Submit Element Value")));
        appCompatButton6.perform(scrollTo(), click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.find_path), withText("Find The Path")));
        appCompatButton7.perform(scrollTo(), click());

    }

}
