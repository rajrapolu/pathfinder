package com.android.mobile.pathfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText rowValue, columnValue, elementValue;
    private Button submitSize, submitElementValue, findPath;
    private TextView assistor, result;

    private int[][] input;

    private int maxRow, maxColumn, row = 0, column = 0;

    private String assistiveText;

    private String inputText = "";

    public boolean invalidMatrix;

    boolean inputedSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //EditText fields
        rowValue = (EditText) findViewById(R.id.row_value);
        columnValue = (EditText) findViewById(R.id.column_value);
        elementValue = (EditText) findViewById(R.id.element_value);

        //Buttons
        submitSize = (Button) findViewById(R.id.submit_size);
        submitElementValue = (Button) findViewById(R.id.submit_element_value);
        findPath = (Button) findViewById(R.id.find_path);

        //TextView
        assistor = (TextView) findViewById(R.id.assistor);
        result = (TextView) findViewById(R.id.result);

        assistiveText = getResources().getString(R.string.element_enter_assist);

        submitSize.setOnClickListener(this);
        submitElementValue.setOnClickListener(this);
        findPath.setOnClickListener(this);
        submitElementValue.setEnabled(false);
        findPath.setEnabled(false);
    }

    @Override
    public void onClick(View view) {
        //Id for submit size button
        if (view.getId() == R.id.submit_size) {
            if (!rowValue.getText().toString().isEmpty() && !columnValue.getText().toString().isEmpty()) {
                maxRow = Integer.valueOf(rowValue.getText().toString());
                maxColumn = Integer.valueOf(columnValue.getText().toString());

                input = new int[maxRow][maxColumn];
                inputedSize = true; //turning it true to make sure that other elements know that input size has been entered
                submitElementValue.setEnabled(true);
                assistor.setText(String.format(assistiveText, row, column));

            } else {
                Toast.makeText(this, R.string.size_message, Toast.LENGTH_SHORT).show();
            }

        //Id of button for submitting the element value
        } else if (view.getId() == R.id.submit_element_value) {
            if (!elementValue.getText().toString().isEmpty() && inputedSize) {

                if (row <= maxRow -1) {
                    if (column < maxColumn -1) {
//                        //input[row][column] = Integer.valueOf(elementValue.getText().toString());
//                        String ii = elementValue.getText().toString().contains("abcdefghijklmnopqrstuvwxyz");

                        try {
                            input[row][column] = Integer.valueOf(elementValue.getText().toString());
                        } catch (NumberFormatException e) {
                            Toast.makeText(this, "Invalid Matrix", Toast.LENGTH_SHORT).show();
                            invalidMatrix = true;
                        }
                        inputText = inputText + "  " + elementValue.getText().toString();
                        column++;
                    } else if (column == maxColumn -1){
                        try {
                            input[row][column] = Integer.valueOf(elementValue.getText().toString());
                        } catch (NumberFormatException e) {
                            Toast.makeText(this, "Invalid Matrix", Toast.LENGTH_SHORT).show();
                            invalidMatrix = true;
                        }
                        inputText = inputText + "  " + elementValue.getText().toString() + "\n";
                        if (row == maxRow -1) {
                            submitElementValue.setEnabled(false);
                            assistor.setText(R.string.input_message);
                            findPath.setEnabled(true);
                            return;
                        }
                        row++;
                        column = 0;
                    }
                    elementValue.setText("");
                }

                assistor.setText(String.format(assistiveText, row, column));

            } else {
                Toast.makeText(this, R.string.element_message, Toast.LENGTH_SHORT).show();
            }

        } else if (view.getId() == R.id.find_path) {
            if (invalidMatrix) {
                result.setText(R.string.invalid_matrix);
            } else {
                String yesOrNo;
                Algorithm algorithm = new Algorithm();
                int leastSum = algorithm.lowestPath(input, maxRow, maxColumn);
                List<Integer> finalPath = algorithm.finalPath();
                boolean reached = algorithm.yesOrNo();
                if (reached) {
                    yesOrNo = getString(R.string.yes);
                } else {
                    yesOrNo = getString(R.string.no);
                }
                String newPath = getString(R.string.path);

                for (int path : finalPath) {
                    newPath = newPath + String.valueOf(path + 1) + " ";
                }
                result.setText(yesOrNo + "\n" + leastSum + "\n" + newPath + "]");
                Toast.makeText(this, yesOrNo + "\n" + leastSum + "\n" + newPath, Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        invalidMatrix = false;
    }
}
