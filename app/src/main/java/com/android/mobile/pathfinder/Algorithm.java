package com.android.mobile.pathfinder;

import java.util.List;

public class Algorithm {
    private int leastSum, leastX, leastY, finalLeastSum = 0, whichOne;
    private Node[][] nodes;
    private boolean finalReached = false;

    //Calculates the lowest path in the given matrix
    public int lowestPath(int[][] array, int sizeX, int sizeY) {
        int sumAbove, sumAlign, sumBelow,above, align, below, leftColumn;

        nodes = new Node[sizeX][sizeY];

        //Creating the node objects
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                Node node = new Node(i, j, array[i][j]);
                nodes[i][j] = node;
            }
        }


        if (sizeY != 1) {
            for (int b = 1; b < sizeY; b++) {
                for (int a = 0; a < sizeX; a++) {

                    //verifying if the values exceeds the limit
                    if (nodes[a][b].getAbove() < 0) {
                        above = sizeX-1;
                    } else {
                        above = nodes[a][b].getAbove();
                    }

                    if (nodes[a][b].getBelow() > sizeX-1) {
                        below = 0;
                    } else {
                        below = nodes[a][b].getBelow();
                    }

                    align = nodes[a][b].getAligned();
                    leftColumn = nodes[a][b].getLeftColumn();

                    //Calculating the sum in three possible ways
                    sumAbove = nodes[a][b].getValue() + nodes[above][leftColumn].getValue();
                    sumAlign = nodes[a][b].getValue() + nodes[align][leftColumn].getValue();
                    sumBelow = nodes[a][b].getValue() + nodes[below][leftColumn].getValue();

                    //Finding out the least sum among the three sums
                    if (sumAbove < sumAlign) {
                        if (sumBelow < sumAbove) {
                            nodes[a][b].setValue(sumBelow);
                            addPath(below, leftColumn, a, b);
                            whichOne = below; //this helps in analyzing which row was able to form the least sum at that particular index position
                            leastSum = sumBelow;
                        } else {
                            nodes[a][b].setValue(sumAbove);
                            addPath(above, leftColumn, a, b);
                            whichOne = above;
                            leastSum = sumAbove;
                        }
                    } else {
                        if (sumAlign < sumBelow) {
                            nodes[a][b].setValue(sumAlign);
                            addPath(align, leftColumn, a, b);
                            whichOne = align;
                            leastSum = sumAlign;
                        } else {
                            nodes[a][b].setValue(sumBelow);
                            addPath(below, leftColumn, a, b);
                            whichOne = below;
                            leastSum = sumBelow;
                        }
                    }

                    //Handling the case of sum greater than 50
                    if (leastSum > 50) {
                        if (nodes[whichOne][leftColumn].getValue() < 50) {
                            leastSum = nodes[whichOne][leftColumn].getValue();
                            finalLeastSum = leastSum;
                            leastX = a;
                            leastY = b;
                        }
                    } else {
                        if (b == sizeY - 1) {
                            if (!finalReached) {
                                finalReached = true;
                                finalLeastSum = nodes[a][b].getValue();
                                leastX = a;
                                leastY = b;
                            }
                            if (finalLeastSum > leastSum) {
                                finalLeastSum = leastSum;
                                leastX = a;
                                leastY = b;
                            }
                            nodes[a][b].addToPath(a);
                        }
                    }
                }
            }
        } else { //Hanlding the case where ther is only one column
            for (int a = 0; a < sizeX - 1; a++) {
                if (array[a][0] < array[a + 1][0]) {
                    leastSum = array[a][0];
                        whichOne = a;
                        leastX = a;
                } else {
                    leastSum = array[a + 1][0];

                        whichOne = a + 1;
                        leastX = a + 1;
                }
            }
            leastY = 0;
            if (leastSum > 50) {
                leastSum = 0;
                nodes[leastX][leastY].addToPath(-1);
            } else {
                finalLeastSum = leastSum;
                finalReached = true;
                nodes[leastX][leastY].addToPath(whichOne);
            }
        }
        return finalLeastSum;
    }

    //Calculates whether the matrix has reached the final column or not
    public boolean yesOrNo() {
        if (finalReached) {
            return true;
        }
        return false;
    }

    //Returns the final path that was traversed
    public List<Integer> finalPath() {
        return nodes[leastX][leastY].getLeastPath();
    }

    //Adds path to the node
    private void addPath(int row, int leftColumn, int presentNodeX, int presentNodeY) {
        if (presentNodeY > 1) {
            for (int previousPath : nodes[row][leftColumn].getLeastPath()) {
                nodes[presentNodeX][presentNodeY].addToPath(previousPath);
            }
        }
        nodes[presentNodeX][presentNodeY].addToPath(row);
    }

}
