package com.android.mobile.pathfinder;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private int i, j, x, y, value;
    private int above, align, below;
    List<Integer> leastPath = new ArrayList<>();

    public Node(int i, int j, int value) {
        this.i = i;
        this.j = j;
        this.value = value;
        above = i - 1;
        align = i;
        below = i + 1;
    }

    public int getRow() {
        return i;
    }

    public int getColumn() {
        return j;
    }

    public int getAbove() {
        return above;
    }

    public int getBelow() {

//        if (below > x - 1) {
//            return 0;
//        }
        return below;
    }

    public int getAligned() {

        return align;
    }

    public int getLeftColumn() {
        return j - 1;
    }

    public int rightColumn() {

        if (j + 1 > 0) {
            return -2;
        }
        return j + 1;
    }

    public int getValue() {
        return value;
    }

    public void addToPath(int path) {
        leastPath.add(path);
    }

    public List<Integer> getLeastPath() {
        return leastPath;
    }

    public void setValue(int least) {
        value = least;
    }
}
