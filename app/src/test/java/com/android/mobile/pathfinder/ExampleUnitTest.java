package com.android.mobile.pathfinder;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.array;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Rule
    public TestName testRule = new TestName();

    @Test
    public void calculate_lowestPath() throws Exception {
        int[][] array = new int[3][4];

        array[0][0] = 51;array[0][1] = 53;array[0][2] = 5;array[0][3] = 51;
        array[1][0] = -2;array[1][1] = 53;array[1][2] = 51;array[1][3] = 51;
        array[2][0] = 51;array[2][1] = -1;array[2][2] = 51;array[2][3] = 51;

        Algorithm algorithm = new Algorithm();
        int least = algorithm.lowestPath(array, 3, 4);
        assertThat(testRule.getMethodName() + " is resulting in an error", 2, equalTo(least));
    }

    @Test
    public void calculate_leastPath() throws Exception {
        int[][] array = new int[3][4];

        array[0][0] = 51;array[0][1] = 1;array[0][2] = 51;array[0][3] = 4;
        array[1][0] = -2;array[1][1] = 51;array[1][2] = 51;array[1][3] = 2;
        array[2][0] = 51;array[2][1] = 51;array[2][2] = 4;array[2][3] = 9;

        Algorithm algorithm = new Algorithm();
        int least = algorithm.lowestPath(array, 3, 4);
        List<Integer> leastPath = algorithm.finalPath();
        assertThat(testRule.getMethodName() + " is resulting in an error", 1, equalTo(leastPath.get(3)));
    }

    @Test
    public void calculate_finalColumnReached() throws Exception {
        int[][] array = new int[3][4];
        int value = 0;

        array[0][0] = 51;array[0][1] = 1;array[0][2] = 51;array[0][3] = 4;
        array[1][0] = -2;array[1][1] = 51;array[1][2] = 51;array[1][3] = 2;
        array[2][0] = 51;array[2][1] = 51;array[2][2] = 4;array[2][3] = 9;

        Algorithm algorithm = new Algorithm();
        int least = algorithm.lowestPath(array, 3, 4);
        boolean yesOrNo = algorithm.yesOrNo();
        if (yesOrNo) {
            value = 1;
        } else {
            value = 0;
        }
        assertThat(testRule.getMethodName() + " is resulting in an error", 1, equalTo(value));
    }
}