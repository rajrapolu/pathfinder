package com.android.mobile.pathfinder;


import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class TestRule implements MethodRule {
    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        final String className = method.getMethod().getDeclaringClass().getSimpleName();
        final String methodName = method.getName();
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                this.evaluate();
            }
        };
    }
}
