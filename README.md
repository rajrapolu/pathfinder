This project helps in identifying the least path in a given matrix.

The input to the project is a matrix and the project outputs the least path, a yes or no based on whether we were able to traverse till the last column, and the path that we have traversed.

Th algorithm that I have used in this project is that I have started at the second column if there exists one, then found out the least path possible to reach a particular element in that column and then traversing till the sum is less than 50 or if we have reached the last column with a least sum. If there is only one column then the row with last value will be printed out if the least sum is less than 50.